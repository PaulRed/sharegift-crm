﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace CrmPackage.Plugins
{
    public static class AutoNumber
    {
        /// <summary>
        /// Calculates next number for an entity
        /// Used locking mechanism to prevent two processes updating at same time
        /// Simple one specific to ShareGift
        /// </summary>
        /// <param name="service">Organisation service</param>
        /// <param name="entName">nme of entity (table name, not label)</param>
        /// <returns>next number as a string</returns>
        public static string GetNextNumber(IOrganizationService service,string entName)
        {
            //get the record with the entity counter it in
            entName = entName.ToLower();
            var queryExpression = new QueryExpression("shr_autonumbercounters")
            {
                ColumnSet = new ColumnSet(true),
                Criteria = new FilterExpression(LogicalOperator.And),
                NoLock = true
            };
            queryExpression.Criteria.AddCondition("shr_entity", ConditionOperator.Equal, entName);
            queryExpression.AddOrder("modifiedon", OrderType.Ascending);

            var counterRec = service.RetrieveMultiple(queryExpression);
            //If not one entity returned then give an error
            if (counterRec.Entities == null || counterRec.Entities.Count != 1)
            {
                throw new Exception("Cannot find autonumber setting for entity " + entName);
            }
            //get counter record
            shr_autonumbercounters countRec= counterRec.Entities.FirstOrDefault().ToEntity<shr_autonumbercounters>();
            countRec.shr_lockfield = new Guid().ToString(); //update with new different value
            service.Update(countRec); //locks it

            //retrieve the value and increment
            int iCountValue = (int)countRec.shr_value + 1;
            //store it back
            countRec.shr_value = iCountValue;
            service.Update(countRec); //update
            //For future cases if system grows. Not in original CRM4 version.
            //If it occurs might globally update existing ones to add two zeroes
            if (iCountValue > 999999) 
            {
                iCountValue.ToString("D8");
            }
            else
            {
                iCountValue.ToString("D6");
            }

            return iCountValue.ToString("D6");
        }
    }
}
