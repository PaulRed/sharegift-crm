// <copyright file="PreValidateOrganisationCreate.cs" company="Microsoft">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Microsoft</author>
// <date>3/24/2015 9:26:40 PM</date>
// <summary>Implements the PreValidateOrganisationCreate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace CrmPackage.Plugins
{
    using System;
    using System.ServiceModel;
    using System.Linq;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;

    /// <summary>
    /// PreValidateOrganisationCreateUpdate Plugin.
    /// </summary>    
    public class PreValidateOrganisationCreateUpdate: Plugin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PreValidateOrganisationCreateUpdate"/> class.
        /// </summary>
        public PreValidateOrganisationCreateUpdate()
            : base(typeof(PreValidateOrganisationCreateUpdate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Create", "account", new Action<LocalPluginContext>(ExecutePreValidateOrganisationCreateUpdate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Update", "account", new Action<LocalPluginContext>(ExecutePreValidateOrganisationCreateUpdate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreValidateOrganisationCreateUpdate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ShareGiftServiceContext svcContext = new ShareGiftServiceContext(service);


            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                Entity entity = (Entity)context.InputParameters["Target"];
                string newNumber = "";

                // Verify that the target entity represents an account.
                // If not, this plug-in was not registered correctly.
                if (entity.LogicalName == "account")    
                {
                    var dupCount = 0;
                    //See if a charity number is there
                    if (entity.Attributes.Contains("sol_charitynumber") && (entity.Attributes["sol_charitynumber"] != null) && (((string)entity.Attributes["sol_charitynumber"]).ToUpper() != "EXEMPT")&& (((string)entity.Attributes["sol_charitynumber"]).ToUpper() != "GENERIC"))
                    {
                        newNumber = (string)entity.Attributes["sol_charitynumber"];
                        if (context.MessageName=="Create")
                        {
                            //Use Linq to find other records
                            dupCount = (from a in svcContext.AccountSet
                                        where a.Sol_CharityNumber == newNumber
                                        select new Account
                                        {
                                            Name = a.Name
                                        }).ToArray().Count();
                        }
                        else //must be update so exclude current record
                        {
                            Guid currentGuid = entity.Id;
                            //Use Linq to find other records
                            dupCount = (from a in svcContext.AccountSet
                                        where (a.Sol_CharityNumber == newNumber) &&
                                        (a.Id != currentGuid)
                                        select new Account
                                        {
                                            Name = a.Name
                                        }).ToArray().Count();
                        }
                    }


                    if (dupCount > 0)
                    {
                        string sMsg = "An organisation record already exists with the charity number '" + newNumber + "'. Enter a unique value into Charity Number or use the word 'Exempt' to exclude this record from checking";
                        throw new InvalidPluginExecutionException(sMsg);

                    }

                }
            } 
        }
    }
}
