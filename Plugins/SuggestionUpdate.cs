﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace CrmPackage.Plugins
{
    public class SuggestionUpdate
    {
        public static void GetContactSuggestionsTotal(ShareGiftServiceContext svcContext, Guid gContactId, Guid newSuggestionId,decimal newSuggestionValue,
            out int iTotalNumber, out Decimal dTotalValue)
        {
            dTotalValue = 0.0M;
            iTotalNumber = 0;

            //Use Linq to get records. Note getting them then totalling as have not found way to do directly
            var suggestions = from s in svcContext.Sol_suggestionSet
                            where s.sol_contactid.Id == gContactId &&
                            s.statuscode.Value == 1
                            select new
                            {
                                Total = s.Sol_SuggestedAmount,
                                Id = s.Sol_suggestionId,
                            };
            //So we have list so do totals etc
            foreach (var s in suggestions)
            {
                if (s.Total != null)
                {
                    dTotalValue += ((Money)s.Total).Value;
                }
                else
                {
                    if (newSuggestionId != Guid.Empty && s.Id == newSuggestionId) //deal with suggestion just created
                    {
                        dTotalValue += newSuggestionValue;
                    }
                }
                iTotalNumber++;
            }
        }

        public static void GetOrganisationSuggestionsTotal(ShareGiftServiceContext svcContext, Guid gOrgId, Guid newSuggestionId, decimal newSuggestionValue,
            out int iTotalNumber, out Decimal dTotalValue)
        {
            dTotalValue = 0.0M;
            iTotalNumber = 0;

            //Use Linq to get records. Note getting them then totalling as have not found way to do directly
            var suggestions = from s in svcContext.Sol_suggestionSet
                              where s.sol_suggestedorganisationid.Id == gOrgId &&
                              s.statuscode.Value == 1 &&
                              s.Sol_SuggestionStatus.Value == 2 //Fix 27/6/16 to only select unused
                              select new
                              {
                                  Total = s.Sol_SuggestedAmount,
                                  Id = s.Sol_suggestionId,
                              };
            //So we have list so do totals etc
            foreach (var s in suggestions)
            {
                if (s.Total != null)
                {
                    dTotalValue += ((Money)s.Total).Value;
                }
                else
                {
                    if (newSuggestionId != Guid.Empty && s.Id == newSuggestionId) //deal with suggestion just created
                    {
                        dTotalValue += newSuggestionValue;
                    }
                }
                iTotalNumber++;
            }
        }


        public static void UpdateSuggestionValues(IPluginExecutionContext context, IOrganizationService service, ShareGiftServiceContext svcContext, Entity entity,Entity preImageEntity)
        {           
            Guid gDonation;
            Guid gThisSuggestion;
            OptionSetValue sSuggestionType;
            Boolean bAllowOverload = false;

            if (context.MessageName == "Update")
            {

                //PreSuggestionImage = (Sol_suggestion)(context.PreEntityImages != null && context.PreEntityImages.Contains("Suggestion") ? context.PreEntityImages["Suggestion"] : null);
                if (preImageEntity != null)
                {
                    //get guid of donation record from preimage
                    if (!preImageEntity.Contains("sol_donationreferenceid")) return; //stop processing plugin as no reference
                    gDonation =  (preImageEntity.GetAttributeValue<EntityReference>("sol_donationreferenceid")).Id;

                    if (!preImageEntity.Contains("sol_suggestiontype")) throw new Exception("Suggestion type missing (amount/percentage)");
                    sSuggestionType = (preImageEntity.GetAttributeValue<OptionSetValue>("sol_suggestiontype"));

                    if (preImageEntity.Contains("sol_allowoverload")) bAllowOverload = ((Boolean)preImageEntity["sol_allowoverload"]);

                    if (!preImageEntity.Contains("sol_suggestionid")) throw new Exception("Suggestion id missing on update message");
                    gThisSuggestion = (preImageEntity.GetAttributeValue<Guid>("sol_suggestionid"));
                }
                else
                {
                    throw new InvalidPluginExecutionException("UpdateSuggestionValues plugin does not have a a Pre-Image called 'Suggestion' associated with the Update step.");
                }
            }

            else //context.MessageName == "Create"
            {
                //get guid of donation record from target
                if (!entity.Contains("sol_donationreferenceid")) return;  //stop processing this function as no reference
                gDonation = (entity.GetAttributeValue <EntityReference>("sol_donationreferenceid")).Id;

                if (!entity.Contains("sol_suggestiontype")) throw new Exception("Suggestion type missing (amount/percentage)");
                sSuggestionType = (entity.GetAttributeValue<OptionSetValue>("sol_suggestiontype"));
                if (entity.Contains("sol_allowoverload")) bAllowOverload = (entity.GetAttributeValue<Boolean>("sol_allowoverload"));
                //gThisSuggestion = Guid.Empty; //not needed in create
                gThisSuggestion = entity.Id;
            }

            //Get donation record
            Sol_sharedonation donation = (Sol_sharedonation)service.Retrieve("sol_sharedonation", gDonation, new ColumnSet("sol_valueofshares")); 
            decimal dDonationValue = donation.Sol_ValueofShares.Value;

            //get value of all other suggestions
            decimal dbOtherSuggestionsPercentageTotal = 0;
            decimal dOtherSuggestionsAmountTotal = 0;

            //Use Linq to get records. Note getting them then totalling as have not found way to do directly
            //if (context.MessageName == "Update")
            {
                var suggestions = from s in svcContext.Sol_suggestionSet
                            where s.sol_donationreferenceid.Id == gDonation &&
                            s.Sol_suggestionId != gThisSuggestion
                            select new
                            {
                                suggamm = s.Sol_SuggestedAmount,
                                suggperc = s.Sol_SuggestedPercentage,
                            };
                //So we have list so do totals etc
                foreach (var s in suggestions)
                {
                    dOtherSuggestionsAmountTotal += ((Money)s.suggamm).Value;
                    dbOtherSuggestionsPercentageTotal += (decimal)s.suggperc;
                }
            }
            //else //crreate *** could we make same as donation
            //{
            //    var suggestions = from s in svcContext.Sol_suggestionSet
            //                where s.sol_donationreferenceid.Id == gDonation
            //                select new
            //                {
            //                    suggamm = s.Sol_SuggestedAmount,
            //                    suggperc = s.Sol_SuggestedPercentage,
            //                };
            //    //So we have list so do totals etc
            //    foreach (var s in suggestions)
            //    {
            //        dOtherSuggestionsAmountTotal += ((Money)s.suggamm).Value;
            //        dbOtherSuggestionsPercentageTotal += (decimal)s.suggperc;
            //    }
            //}

            double dSuggestionPercentage = 0;
            double dNewPercentage = 0;
            decimal dSuggestionAmount = 0;
            decimal dNewValue = 0;

            if (sSuggestionType.Value == 2) //if percentage - (not good to have literal value but have not added extension to crmsvcutil to add option set constants
            {
                        //get percentage value from target or if not there, then get from pre-entity image, else zero
                dSuggestionPercentage = (entity.Contains("sol_suggestedpercentage")) ? entity.GetAttributeValue<double>("sol_suggestedpercentage") : (preImageEntity.Contains("sol_suggestedpercentage") ? preImageEntity.GetAttributeValue<double>("sol_suggestedpercentage") : 0);

                        //if % then calc value as (total * (% / 100))
                        dNewValue = Convert.ToDecimal((Convert.ToDouble(dDonationValue) * (dSuggestionPercentage / 100)));

                        //add value to all other suggestions total
                        decimal dAllSuggestions = dOtherSuggestionsAmountTotal + dNewValue;

                        //if > value of donation throw exception
                        if (!bAllowOverload && (dAllSuggestions > dDonationValue)) throw new InvalidPluginExecutionException("Failed: The sum of all suggestion values for the selected donation cannot be greater than the donation value." + Environment.NewLine + Environment.NewLine + "Sum of all suggestions (including this record): " + dAllSuggestions.ToString("C") + Environment.NewLine + "Donation value: " + dDonationValue.ToString("C"));

                        //else update record properties to include the missing value
                        entity.Attributes["sol_suggestedamount"]= new Microsoft.Xrm.Sdk.Money(dNewValue);
            }
                    else
            {
                        //get amount from target or if not there, then get from pre-entity image, else zero
                dSuggestionAmount = (entity.Contains("sol_suggestedamount")) ? entity.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("sol_suggestedamount").Value : (preImageEntity.Contains("sol_suggestedamount") ? preImageEntity.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("sol_suggestedamount").Value : 0);

                         //if value then calc % as ((value * 100) / total)
                        dNewPercentage = Convert.ToDouble(((dSuggestionAmount * 100) / dDonationValue));

                        //add value to all other suggestions total
                        decimal dAllSuggestions = dOtherSuggestionsAmountTotal + dSuggestionAmount;

                        //if > value of donation throw exception
                        if (!bAllowOverload && (dAllSuggestions > dDonationValue)) throw new InvalidPluginExecutionException("Failed: The sum of all suggestion values for the selected donation cannot be greater than the donation value." + Environment.NewLine + Environment.NewLine + "Sum of all suggestions (including this record): " + dAllSuggestions.ToString("C") + Environment.NewLine + "Donation value: " + dDonationValue.ToString("C"));

                        //else update record properties to include the missing value
                        entity.Attributes["sol_suggestedpercentage"] = dNewPercentage;
            }
            context.InputParameters["Target"] = entity;
            //service.Update(entity);
        }
        /// <summary>
        /// Updates suggestion totals on a contact record
        /// </summary>
        /// <param name="service">Organisation service</param>
        /// <param name="gContactId">contact Guid</param>
        /// <param name="iTotalNumber">Total number of suggestions</param>
        /// <param name="dTotalValue">Total value suggestions</param>
        public static void UpdateContactTotals(IOrganizationService service, Guid gContactId,
             int iTotalNumber, Decimal dTotalValue)
        {
            Contact retrievedContact = (Contact)service.Retrieve("contact", gContactId, new ColumnSet("sol_numofsuggestions","sol_suggestionvalue"));
            retrievedContact.Sol_numofsuggestions = iTotalNumber;
            retrievedContact.Sol_SuggestionValue = new Microsoft.Xrm.Sdk.Money(dTotalValue);
            service.Update(retrievedContact);
        }

        /// <summary>
        /// Updates suggestion totals on an organisation record
        /// </summary>
        /// <param name="service">Organisation service</param>
        /// <param name="gOrgId">Organisation Guid</param>
        /// <param name="iTotalNumber">Total number of suggestions</param>
        /// <param name="dTotalValue">Total value suggestions</param>
        public static void UpdateOrganisationTotals(IOrganizationService service, Guid gOrgId,
             int iTotalNumber, Decimal dTotalValue)
        {
            Account retrievedAccount = (Account)service.Retrieve("account", gOrgId, new ColumnSet("sol_numofsuggestions", "sol_suggestionvalue"));
            retrievedAccount.Sol_numofsuggestions = iTotalNumber;
            retrievedAccount.Sol_SuggestionValue = new Microsoft.Xrm.Sdk.Money(dTotalValue);
            service.Update(retrievedAccount);
        }

    }
}
