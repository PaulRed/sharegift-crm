﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CrmPackage.Plugins
{
    //Holds utilities to remove characters from folder and file names which are not allowed in SharePoint
    static class FileandFolderNameUtilities
    {
        public static string RemoveIllegalFolderNameCharacters(string sourceFolder)
        {
            string invalid = new string(Path.GetInvalidPathChars()) + @"""#%&*:<>?\/{}|~+-,()."; //invalid file name plus extras for sharepoint folders
            for (int i = 0; i < 32; i++) { invalid += ((char)i).ToString(); }
            foreach (char c in invalid) { sourceFolder = sourceFolder.Replace(c.ToString(), ""); }
            if (sourceFolder.StartsWith("_")) { sourceFolder = sourceFolder.Substring(1); }; //leading _ 
            sourceFolder = sourceFolder.Trim();
            sourceFolder = sourceFolder.Replace("  ", " "); //get rid of double spaces
            if (sourceFolder.Length > 50) { sourceFolder = sourceFolder.Substring(0, 50); }
            return sourceFolder.Trim();
        }

        public static string RemoveIllegalFileNameCharacters(string sourceFile)
        {
            string invalid = new string(Path.GetInvalidFileNameChars()) + @"\""#%&*:<>?/{|},";
            for (int i = 0; i < 32; i++) { invalid += ((char)i).ToString(); }
            foreach (char c in invalid) { sourceFile = sourceFile.Replace(c.ToString(), ""); }
            sourceFile = sourceFile.Trim();
            sourceFile = sourceFile.Replace("  ", " "); //get rid of double spaces
            return sourceFile.Trim();
        }

        public static string DeriveFolderName(string name, Guid guid)
        {
            string str = RemoveIllegalFolderNameCharacters(name);
            if (str.Length > 29) { str = str.Substring(0, 29); }
            str = str + "_" + guid.ToString().Replace("-", "");
            if (str.Length > 50) { str = str.Substring(0, 50); }
            return str;
        }

    }
}
