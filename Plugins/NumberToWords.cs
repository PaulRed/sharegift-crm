﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmPackage.Plugins
{
    /// <summary>
    /// Has methods to convert numbers to words
    /// </summary>
    public static class NumberToWordConversions
    {
         
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "Zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        /// <summary>
        /// Converts a number which represents a number of pounds to a descriptive string
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string PoundsToString(double d)
        {
            if (d == 1.0D)
            {
                return "One pound only";
            }
            int iPence = (int)((d - Math.Truncate(d)) * 100);
            string s = NumberToWords((int)d);
            if (iPence == 0)
            {
                s += " pounds only";
            }
            else
                s += " pounds and " + NumberToWords(iPence) + " pence";

            return (char.ToUpper(s[0]) + s.Substring(1)).Replace("  ", " ");
        }

        /// <summary>
        /// Converts a number which represents a number of units to a descriptive string. Decimals if present are arounded to 2 decimal places.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string UnitsToString(double d)
        {
            if (d == 1.0D)
            {
                return "One";
            }

            int iDecimals = (int)((Math.Round(d - Math.Truncate(d), 2) * 100.0));
            string s = NumberToWords((int)d);
            if (iDecimals > 0)
                s += " point " + iDecimals.ToString("00");

            return (char.ToUpper(s[0]) + s.Substring(1)).Replace("  ", " ");
        }
    }
}
