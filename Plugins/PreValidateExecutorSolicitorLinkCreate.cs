// <copyright file="PreValidateExecutorSolicitorLinkCreate.cs" company="Microsoft">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Microsoft</author>
// <date>6/8/2015 7:12:12 AM</date>
// <summary>Implements the PreValidateExecutorSolicitorLinkCreate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace CrmPackage.Plugins
{
    using System;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;

    /// <summary>
    /// PreValidateExecutorSolicitorLinkCreate Plugin.
    /// </summary>    
    public class PreValidateExecutorSolicitorLinkCreate: Plugin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PreValidateExecutorSolicitorLinkCreate"/> class.
        /// </summary>
        public PreValidateExecutorSolicitorLinkCreate()
            : base(typeof(PreValidateExecutorSolicitorLinkCreate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Create", "sol_executor", new Action<LocalPluginContext>(ExecutePreValidateExecutorSolicitorLinkCreate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreValidateExecutorSolicitorLinkCreate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ShareGiftServiceContext svcContext = new ShareGiftServiceContext(service);

            bool bPrimarySelected = false;
            Guid gExecutor = new Guid();


            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                Entity entity = (Entity)context.InputParameters["Target"];
                // Verify that the target entity represents a an executor.
                // If not, this plug-in was not registered correctly.
                if (entity.LogicalName == "sol_executor")
                {
                    //get the fields from the executor record
                    Sol_executor executor = entity.ToEntity<Sol_executor>();
                    //Sol_executor executor = (Sol_executor)service.Retrieve("sol_executor", entity.Id, new ColumnSet("sol_primaryexecutor", "sol_executorcontactid")); 
                    bPrimarySelected = (bool)executor.Sol_PrimaryExecutor;
                    gExecutor = executor.sol_executorcontactid.Id;

                    if (bPrimarySelected && (gExecutor != null))  
                    {
                        //Check address is present
                        Contact retrievedContact = (Contact)service.Retrieve("contact", gExecutor, new ColumnSet("address1_postalcode", "address1_line1"));
                        if (!(retrievedContact.Contains("address1_postalcode") && (retrievedContact.Address1_PostalCode != "")) && retrievedContact.Contains("address1_line1") && (retrievedContact.Address1_Line1 != ""))
                        {
                            throw new InvalidPluginExecutionException("This contact is unable to be a primary executor as there is no address stored." + Environment.NewLine + Environment.NewLine + "(Minimum information required: Street 1 & Postcode)");
                        }
                    }
                }
            }
        }
    }
}
