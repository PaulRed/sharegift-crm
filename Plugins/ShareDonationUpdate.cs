﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace CrmPackage.Plugins
{
    public class ShareDonationUpdate
    {
        public static void  GetActiveShareDonationsTotal(ShareGiftServiceContext svcContext,Guid gContactId,
            out int iTotalNumber, out Decimal dTotalValue, ITracingService tracingService)
        {
            //tracingService.Trace("GetActiveShareDonationsTotal starting for contact " + gContactId.ToString());
            dTotalValue = 0.0M;
            iTotalNumber = 0;
            
            //Use Linq to get records. Note getting them then totalling as have not found way to do directly
            var donations = from s in svcContext.Sol_sharedonationSet
                            where s.sol_contactid.Id == gContactId &&
                            s.statuscode.Value == 1
                            select new
                            {
                                Total = s.Sol_ValueofShares,
                                DonationType = s.Sol_DonationType
                            };
            //So we have list so do totals etc
            foreach (var s in donations)
            {
                //if (s.Total !=null) {tracingService.Trace("GetActiveShareDonationsTotal one donation total: " + s.Total.ToString());}
                //else {tracingService.Trace("GetActiveShareDonationsTotal Total is null");}
                //if (s.DonationType != null) { tracingService.Trace("GetActiveShareDonationsTotal one donation type: " + s.DonationType.ToString()); }
                //else { tracingService.Trace("GetActiveShareDonationsTotal DonationType is null"); }
                    
                if (s.Total != null && s.DonationType != null)
                {
                    dTotalValue += ((Money)s.Total).Value;
                    iTotalNumber++;
                }
            }
            //tracingService.Trace("GetActiveShareDonationsTotal totals are " + iTotalNumber.ToString() + " " + dTotalValue.ToString());     
        }

        /// <summary>
        /// Updates share donation totals on a contact organisation record
        /// </summary>
        /// <param name="service">Organisation service</param>
        /// <param name="gContactId">contact Guid</param>
        /// <param name="iTotalNumber">Total number of donations</param>
        /// <param name="dTotalValue">Total value donated</param>
        public static void UpdateContactTotals(IOrganizationService service, Guid gContactId,
             int iTotalNumber, Decimal dTotalValue, ITracingService tracingService)
        {
            //tracingService.Trace("UpdateContactTotals start"); 
            Contact retrievedContact = (Contact)service.Retrieve("contact", gContactId, new ColumnSet("sol_totalnoofactivesharedonations", "sol_totalvalueofactivesharedonations"));
            retrievedContact.Sol_TotalNoofActiveShareDonations = iTotalNumber;
            retrievedContact.Sol_TotalValueofActiveShareDonations = new Microsoft.Xrm.Sdk.Money(dTotalValue);
            service.Update(retrievedContact);
            //tracingService.Trace("UpdateContactTotals end"); 
        }

    }
}
