// <copyright file="PostSuggestionUpdate.cs" company="Microsoft">
// Copyright (c) 2015 All Rights Reserved
// </copyright>
// <author>Microsoft</author>
// <date>6/18/2015 9:02:04 AM</date>
// <summary>Implements the PostSuggestionUpdate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace CrmPackage.Plugins
{
    using System;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    /// <summary>
    /// PostSuggestionUpdate Plugin.
    /// Fires when the following attributes are updated:
    /// All Attributes
    /// </summary>    
    public class PostSuggestionUpdate: Plugin
    {
        /// <summary>
        /// Alias of the image registered for the snapshot of the 
        /// primary entity's attributes before the core platform operation executes.
        /// The image contains the following attributes:
        /// No Attributes
        /// </summary>
        private readonly string preImageAlias = "Suggestion";

        /// <summary>
        /// Initializes a new instance of the <see cref="PostSuggestionUpdate"/> class.
        /// </summary>
        public PostSuggestionUpdate()
            : base(typeof(PostSuggestionUpdate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "sol_suggestion", new Action<LocalPluginContext>(ExecutePostSuggestionUpdate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostSuggestionUpdate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }
            IPluginExecutionContext context = localContext.PluginExecutionContext;

            Entity preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains(this.preImageAlias)) ? context.PreEntityImages[this.preImageAlias] : null;

            IOrganizationService service = localContext.OrganizationService;
            ShareGiftServiceContext svcContext = new ShareGiftServiceContext(service);

            Entity entity = (Entity)context.InputParameters["Target"];

            //Update the percentage/amount values
            SuggestionUpdate.UpdateSuggestionValues(context, service, svcContext, entity, preImageEntity);

            // Verify that the target entity represents a suggestion.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName == "sol_suggestion")
            {

                Sol_suggestion suggestion = (Sol_suggestion)service.Retrieve("sol_suggestion", entity.Id, new ColumnSet("sol_suggestedorganisationid", "sol_contactid"));
                Guid gOrganisationId = (entity.Contains("sol_suggestedorganisationid") ? (entity.GetAttributeValue<EntityReference>("sol_suggestedorganisationid")).Id : suggestion.sol_suggestedorganisationid.Id); //take updated one or if not present one to current entity
                Guid gContactId = (entity.Contains("sol_contactid") ? (entity.GetAttributeValue<EntityReference>("sol_contactid")).Id : suggestion.sol_contactid.Id);

                if (gOrganisationId != Guid.Empty && gOrganisationId != null)
                {
                    decimal dTotalAmount;
                    int iTotalNumber;
                    SuggestionUpdate.GetOrganisationSuggestionsTotal(svcContext, gOrganisationId, Guid.Empty, 0, out  iTotalNumber, out dTotalAmount);
                    SuggestionUpdate.UpdateOrganisationTotals(service, gOrganisationId, iTotalNumber, dTotalAmount);
                }
                if (gContactId != Guid.Empty && gContactId != null)
                {
                    decimal dTotalAmount;
                    int iTotalNumber;
                    SuggestionUpdate.GetContactSuggestionsTotal(svcContext, gContactId, Guid.Empty, 0, out  iTotalNumber, out dTotalAmount);
                    SuggestionUpdate.UpdateContactTotals(service, gContactId, iTotalNumber, dTotalAmount);
                }

                //Update previous record if different
                Guid gPreContactId = (preImageEntity.Contains("sol_contactid") ? (preImageEntity.GetAttributeValue<EntityReference>("sol_contactid")).Id : Guid.Empty);
                Guid gPreOrganisationId = (preImageEntity.Contains("sol_suggestedorganisationid") ? (preImageEntity.GetAttributeValue<EntityReference>("sol_suggestedorganisationid")).Id : Guid.Empty);

                if ((gPreOrganisationId != Guid.Empty) && ((gOrganisationId != gPreOrganisationId) || (gOrganisationId == Guid.Empty)))
                {
                    decimal dTotalAmount;
                    int iTotalNumber;
                    SuggestionUpdate.GetOrganisationSuggestionsTotal(svcContext, gPreOrganisationId, Guid.Empty, 0, out  iTotalNumber, out dTotalAmount);
                    SuggestionUpdate.UpdateOrganisationTotals(service, gPreOrganisationId, iTotalNumber, dTotalAmount);
                }
                if ((gPreContactId != Guid.Empty) && ((gContactId != gPreContactId) || (gContactId == Guid.Empty)))
                {
                    decimal dTotalAmount;
                    int iTotalNumber;
                    SuggestionUpdate.GetContactSuggestionsTotal(svcContext, gPreContactId, Guid.Empty, 0, out  iTotalNumber, out dTotalAmount);
                    SuggestionUpdate.UpdateContactTotals(service, gPreContactId, iTotalNumber, dTotalAmount);
                }       
            }
        }
    }
}
