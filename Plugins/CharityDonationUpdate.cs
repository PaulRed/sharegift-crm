﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace CrmPackage.Plugins
{
    public class CharityDonationUpdate
    {
        public static void GetSummaryOrgCharityDonationsAmountTotal(ShareGiftServiceContext svcContext,Guid gOrganisationId, out Decimal dTotalAmount,
            out int iTotalNumber, out Decimal dLastAmount, out DateTime dtLastDate)
        {
            dTotalAmount = 0.0M;
            dLastAmount = 0.0M;
            iTotalNumber = 0;
            dtLastDate = DateTime.Parse("1900-01-01"); //default is a long time ago
            
            //Use Linq to get records. Note getting them then totalling as have not found way to do directly
            var donations = from s in svcContext.Sol_charitydonationSet
                            where s.sol_organisationid.Id == gOrganisationId &&
                            s.statuscode.Value == 1
                            orderby s.Sol_Date
                            select new
                            {
                                Total = s.Sol_Amount,
                                Date = s.Sol_Date

                            };
            //So we have list so do totals etc
            foreach (var s in donations)
            {
                iTotalNumber++;
                //Occasionally amount might be null
                if (s.Total != null)
                {
                    dLastAmount = ((Money)s.Total).Value;
                    dTotalAmount += dLastAmount;
                    if (s.Date != null)
                    {
                        dtLastDate = (DateTime)s.Date;
                    }
                    dLastAmount = ((Money)s.Total).Value;
                }
            }     
        }

        /// <summary>
        /// Updates charity donation totals on a charity organisation record
        /// </summary>
        /// <param name="service">Organisation service</param>
        /// <param name="gOrganisationId">Charity organisation Guid</param>
        /// <param name="dTotalAmount">Total amount donated</param>
        /// <param name="iTotalNumber">Total number of donations</param>
        /// <param name="dLastAmount">Most recent amount given</param>
        /// <param name="dtLastDate">Date of last donation</param>
        public static void UpdateOrganisationTotals(IOrganizationService service,Guid gOrganisationId,  Decimal dTotalAmount,
             int iTotalNumber,  Decimal dLastAmount,  DateTime dtLastDate)
        {
            Account retrievedAccount = (Account)service.Retrieve("account", gOrganisationId,new ColumnSet( "sol_numberofcharitydonations", "sol_valueofcharitydonations","sol_dateoflastdonation","sol_amountoflastdonation"));
            retrievedAccount.Sol_NumberofCharityDonations = iTotalNumber;
            retrievedAccount.Sol_ValueofCharityDonations = new Microsoft.Xrm.Sdk.Money(dTotalAmount);
            retrievedAccount.Sol_AmountofLastDonation = new Microsoft.Xrm.Sdk.Money(dLastAmount);
            retrievedAccount.Sol_DateofLastDonation = dtLastDate;
            service.Update(retrievedAccount);
        }

    }
}
